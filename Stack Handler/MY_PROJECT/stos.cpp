#include "stdafx.h"


size_t MY_STACK::stack_size = 0; //incijowanie ostatniego
static MY_STACK *pierwszy = NULL;  //wskaznik do pierwszego elementu w stosie

FreeData ptr_free_dat; //deklaracja wskaznika do funkcji typu FreeData
CompareData ptr_compare_dat;
WriteDataFile ptr_write_dat;
PrintData ptr_print_dat;
ReadDataFile ptr_read_dat;

void MY_STACK_Initialize(FreeData pFreeDat, WriteDataFile pWrireData, CompareData pCompareDat, PrintData pPrintDat, ReadDataFile pReadDat)
{
	pierwszy = NULL;
	ptr_free_dat = pFreeDat;
	ptr_write_dat = pWrireData; 
	ptr_compare_dat = pCompareDat; 
	ptr_print_dat = pPrintDat; 
	ptr_read_dat = pReadDat; 
	pierwszy->stack_size = 0;
}

void MY_STACK_Free()
{
	
	MY_STACK *p = pierwszy, *ptmp = NULL;

while (p)
	{
	

		(*ptr_free_dat)(p->pData);

		ptmp = p;

	
		p = p->next;

		
		free(ptmp);
		printf("Usunieto element\n");
	}

	pierwszy = NULL;
	pierwszy->stack_size = 0;
}



MY_STACK * MY_STACK_Push(void *pdat)
{
	MY_STACK *nowy = (MY_STACK *)malloc(sizeof(MY_STACK));
	if (!nowy)
		moje_bledy(MOJE_KOMUNIKATY_allocate_error);
	nowy->pData = pdat;
	nowy->next = pierwszy;
	pierwszy = nowy;
	size_t ilosc = pierwszy->stack_size;
	pierwszy->stack_size = ++ilosc;
	printf("Dodano element\n");
	return pierwszy;
}

MY_STACK MY_STACK_Pop()
{
	MY_STACK rv;
	if (!pierwszy)
	{
		printf("==========================================\n");
		printf("STOS JEST PUSTY\n");
		printf("==========================================\n");
		rv.pData = NULL;
		rv.next = NULL;
	}
	else
	{
		printf("==========================================\n");
		
		MY_STACK *nastepny = pierwszy->next;
		rv.pData = pierwszy->pData;
	    rv.next = NULL;
		size_t ilosc = pierwszy->stack_size;
		pierwszy->stack_size = --ilosc;
		
		free(pierwszy); 
		pierwszy = nastepny;
		if (pierwszy == NULL)
			printf("STOS WYCZYSZCZONY, JEGO ROZMIAR TO %zd\n", pierwszy->stack_size);
	}

	return rv;

}

MY_STACK * MY_STACK_FirstElem() { 
	MY_STACK * tmp = NULL;
	if (!pierwszy) {
		printf("Stos jest aktualnie pusty\n");
		tmp->pData = NULL;
		tmp->next = NULL;
	}
	else {
		tmp = pierwszy;
	}
	return tmp;
}

void MY_STACK_FindElem(char *slowo) {
	MY_STACK * tmp = MY_STACK_FirstElem(); 
	int b;
	while (tmp)
	{ 
		(*ptr_compare_dat)(tmp->pData, slowo, &b);
		tmp = tmp->next;
	}
	if (!b)
		moje_bledy(MOJE_KOMUNIKATY_matching_element_error);

	
}

void MY_STACK_PrintAllElem() {
	MY_STACK * tmp = pierwszy;
		
	size_t ilosc = pierwszy->stack_size;

	while (tmp)
	{
		
		printf("==========================================\n");
		if (tmp->pData)
			
			(*ptr_print_dat)(tmp->pData);
		

		tmp = tmp->next;
		printf("==========================================\n");
	}
	printf("ELEMENTY NA STOSIE = %zd\n", ilosc);
	printf("WYSWIETLONO WSZYSTKO \n\n");
}


void MY_STACK_WriteStack(char filename[]) {

		MY_STACK *tmp = pierwszy
			;
		// zapisujemy liczbe el. stosu
		size_t liczba_el = pierwszy->stack_size;
		size_t it;

		FILE *plik = NULL;

		fopen_s(&plik, filename, "wb");       // wb - zapis
		if (!plik)
			moje_bledy(MOJE_KOMUNIKATY_open_file_error);
		
		if (!fwrite(&liczba_el, sizeof(size_t), 1, plik))
			moje_bledy(MOJE_KOMUNIKATY_save_error);

		for (it = 0; it < liczba_el; ++it)
		{
			(*ptr_write_dat)(tmp->pData, plik);
			tmp = tmp->next;
		}

		if (plik)
			fclose(plik);
		plik = NULL;

		printf("\nZapisano do pliku.\n\n");
	}


void MY_STACK_ReadStack(char filepathname[]) 
{

		MY_STACK_Free();

		void *curr;
		size_t liczba_el = 0, it;

		FILE *plik = NULL;
		fopen_s(&plik, filepathname, "rb");       // rb - odczytywanie

		if (!fread(&liczba_el, sizeof(size_t), 1, plik))
			moje_bledy(MOJE_KOMUNIKATY_read_error);

		for (it = 0; it < liczba_el; ++it)
		{
			curr = ptr_read_dat(plik);
			MY_STUDENT_Print(curr);
			MY_STACK_Push(curr);
		}

		if (plik)
			fclose(plik);
		plik = NULL;

		printf("\nOdczytano z pliku.\n\n");
	}


#ifndef MY_STACK_H
#define MY_STACK_H

typedef void(*FreeData)(void *pdat);
typedef void(*WriteDataFile)(void *pdat, FILE *plik);
typedef void(*CompareData)(void *pdat, char *string, int *ret);
typedef void(*PrintData)(void *pdat);
typedef void * (*ReadDataFile)(FILE *plik);

struct MY_STACK
{
	void *pData;    
	MY_STACK *next; 
	static size_t stack_size;
};


void MY_STACK_Initialize(FreeData pFreeDat, WriteDataFile pWriteData, CompareData pCompareDat, PrintData pPrintDat, ReadDataFile pReadDat);
void MY_STACK_Free();
MY_STACK * MY_STACK_Push(void *pdat);
MY_STACK MY_STACK_Pop();
MY_STACK * MY_STACK_FirstElem();
void MY_STACK_FindElem(char * string);
void MY_STACK_PrintAllElem();
void MY_STACK_WriteStack(char filepathname[]);
void MY_STACK_ReadStack(char filepathname[]);

#endif
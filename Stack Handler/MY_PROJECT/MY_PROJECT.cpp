#include "stdafx.h"
#include "interfejs.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int _tmain(int argc, _TCHAR* argv[])
{
	MY_STACK_Initialize(MY_STUDENT_Free, MY_STUDENT_Write, MY_STUDENT_Compare, MY_STUDENT_Print, MY_STUDENT_Read); //wyjaśnienie stosu dla elementu studenta

	char filepathname[_MAX_PATH];
	size_t op = 0;

	_info();
	workdir(filepathname); //sciezka
	
	while (op >= PUSH && op <= STOP) //definiowanie liczby iteracji po enumach w interfejs.h
	{
		_menu();
		scanf_s("%zd", &op);
		switch (op)
		{
			//polecenia w menu
		case PUSH: _push();
			break;
		case POP: _pop();
			break;
		case WRITE: _write(filepathname);
			break;
		case READ: _read(filepathname);
			break;
		case ALL: _printall();
			break;
		case FIND: _find();
			break;
		case CLEAR: _clear();
			break;
		case STOP: 
			
			system("pause");
			return 0;
		default:
			printf("Nie ma takiej operacji\n");
		};
	}

	system("pause");
	
	return 0;
}


#include "stdafx.h"
#include "bledy.h"

static char *MOJE_KOMUNIKATY_tablica[] = {
	"EBlad alokacji pameici",				
	"EBlad ---KOPIOWANIE---",
	"EBlad ---ZAPIS DO PLIKU---",
	"EBlad ---OTWARCIE PLIKU---",
	"XBrak pasujacego elementu",
	"EBlad ---ODCZYT Z PLIKU---",
	"XKatalog istnieje",
	"XPodsumowanie"				
};

MOJE_DECYZJE moje_bledy(MOJE_KOMUNIKATY message) {
	MOJE_DECYZJE retval = MOJE_DECYZJE_CONTINUE; 
	puts(MOJE_KOMUNIKATY_tablica[message] + 1); 
	if (MOJE_KOMUNIKATY_tablica[message][0] == 'E') {
		
		retval = MOJE_DECYZJE_BREAK;
	}
	else if (MOJE_KOMUNIKATY_tablica[message][0] == 'X') {
		retval = MOJE_DECYZJE_WARNING;
		
	}

	return retval;
}
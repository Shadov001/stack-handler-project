#include "stdafx.h"
#include <direct.h>
#include "workdir.h"

//#define TXT

#ifdef TXT
const char rozszerzenie[] = ".txt";
#else
const char rozszerzenie[] = ".bin";
#endif

void workdir(char filepathname[])
{
	char filedrive[_MAX_DRIVE],
		filedir[_MAX_DIR],
		filename[_MAX_FNAME],
		fileext[_MAX_EXT];

	printf("Trwa tworzenie katalogu Path\n");


	_getcwd(filepathname, _MAX_PATH * sizeof(char));

	strcat_s(filepathname, _MAX_PATH * sizeof(char), "\\Path");
	int retval = _mkdir(filepathname);
	if (retval)
	{
		moje_bledy(MOJE_KOMUNIKATY_cat_already_exist);
	}

	strcat_s(filepathname, _MAX_PATH * sizeof(char), "\\myfile");

	strcat_s(filepathname, _MAX_PATH * sizeof(char), rozszerzenie);


	_splitpath_s(filepathname, filedrive, sizeof(filedrive),
		filedir, sizeof(filedir),
		filename, sizeof(filename),
		fileext, sizeof(fileext));

	memset(filepathname, 0, _MAX_PATH * sizeof(char)); 
	_makepath_s(filepathname, _MAX_PATH * sizeof(char), filedrive, filedir, filename, fileext);//sklejam nazwe pliku
}


//////////////////////////////////////////////////////////////////////
//  my_interface.cpp

#include "stdafx.h"

#pragma warning (disable : 4996)

static char *pol[] =
{
	"0. Push element",			
	"1. Pop element",		
	"2. Write stack",		
	"3. Read stack ",			
	"4. Print all elements",	
	"5. Find element by student name",			
	"6. Clear stack",		
	"7. Finish"       
};
void _info() {
	printf("|OBSLUGA STOSU| \n AUTOR: |Marcin Jamroz|  |GRUPA 11|  |ALBUM 121030|\n");

}
void _menu()
{
	size_t it;
	for (it = 0; it < TOT; ++it)
	{
		printf("%s\n", pol[it]);
	}
}

void _push()
{
    _std_clean();
	char nazwisko_tmp[512];
	size_t rok;
	static int random_kierunek = 1;
	enum KIERUNEK kierunek = KIERUNEK_INFORMATYKA;
	printf("Podaj: NAZWISKO, ROK, KIERUNEK-AUTOMATYCZNIE\n");
	printf("|Nazwisko|:\n");
	
	gets_s(nazwisko_tmp, sizeof(nazwisko_tmp));

	size_t rozmiar_nazwisko = strlen(nazwisko_tmp);
	char *nazwisko = (char *)malloc((rozmiar_nazwisko + 1) * sizeof(char));   //SF a gdzie Pan to zwalnia? (POPRAWIONE)
	if (!nazwisko) {
		moje_bledy(MOJE_KOMUNIKATY_allocate_error);
	}
	void* ret = memcpy(nazwisko, nazwisko_tmp, (rozmiar_nazwisko + 1) * sizeof(char));
	if (ret == (void*)NULL)
		moje_bledy(MOJE_KOMUNIKATY_copy_error);
	printf("|Rok|:\n");
	if (!scanf("%zd", &rok)) {
		moje_bledy(MOJE_KOMUNIKATY_push_error);
	}
	if (random_kierunek == 4)
		random_kierunek = 1;
	
	if (random_kierunek == 1)
		kierunek = KIERUNEK_FIZYKA;
	
	if (random_kierunek == 2)
		kierunek = KIERUNEK_INFORMATYKA;
	
	if (random_kierunek == 3)
		kierunek = KIERUNEK_MATEMATYKA; 
	
	random_kierunek++;
	

	void *pdat = MY_STUDENT_Push(nazwisko, (rozmiar_nazwisko + 1) * sizeof(char), rok, kierunek);
	if (!MY_STACK_Push(pdat))
		printf("PUSH ERROR!\n");

	if (nazwisko)
	    free(nazwisko);
	nazwisko = NULL;
}

void _pop()
{
	MY_STACK tmp = MY_STACK_Pop();
	MY_STUDENT_Print(tmp.pData);
	MY_STUDENT_Free(tmp.pData);
}

void _find()
{
	char search_data[512];
	_std_clean();

	printf("Nazwisko, ktorego szukasz:\n");
	gets_s(search_data, sizeof(search_data));

	MY_STACK_FindElem(search_data);
	
}

void _printall() 
{
	MY_STACK_PrintAllElem();
}

void _std_clean() 
{
	int ch;
	printf("Czyszcze stdin: ");
	while ((ch = getchar()) != '\n')
	{
		printf("%c", ch);
	}
	printf(" ; \n");
}

void _read(char filepathname[]) 
{
    MY_STACK_ReadStack(filepathname);
}

void _write(char fpname[]) 
{
    MY_STACK_WriteStack(fpname);
}

void _clear()
{
	MY_STACK_Free();
}
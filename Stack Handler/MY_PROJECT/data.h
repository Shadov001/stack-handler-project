///////////////////////////////////////////////////////////

#ifndef MY_DATA_ADFGRETW__H
#define MY_DATA_ADFGRETW__H

enum KIERUNEK {
	KIERUNEK_INFORMATYKA,
	KIERUNEK_MATEMATYKA,
	KIERUNEK_FIZYKA
};

struct MY_STUDENT
{
	size_t rok;
	char *nazwisko; 
	enum KIERUNEK kierunek;
	size_t rozmiar_nazw;

	
};
void MY_STUDENT_kierunek(enum KIERUNEK mess);
void * MY_STUDENT_Read(FILE *plik);
void MY_STUDENT_Write(void *ptr, FILE *plik);
void MY_STUDENT_Free(void *ptr);
void * MY_STUDENT_Initialize(char * nname, size_t rozmiarnname, size_t rok, KIERUNEK kierunek);
void * MY_STUDENT_Push(char *nazwisko, size_t rozmiarnazwisko, size_t rok, enum KIERUNEK kierunek);
void MY_STUDENT_Compare(void *ptr, char * string, int *retvalue);
void MY_STUDENT_Print(void *ptr);

#endif
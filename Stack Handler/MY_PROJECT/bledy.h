#pragma once
#ifndef BLEDY_KOMUNIKACJI__H
#define BLEDY_KOMUNIKACJI__H

enum MOJE_KOMUNIKATY {
	MOJE_KOMUNIKATY_allocate_error, 
	MOJE_KOMUNIKATY_copy_error,
	MOJE_KOMUNIKATY_push_error,
	MOJE_KOMUNIKATY_save_error,
	MOJE_KOMUNIKATY_open_file_error,
	MOJE_KOMUNIKATY_matching_element_error,
	MOJE_KOMUNIKATY_read_error,
	MOJE_KOMUNIKATY_cat_already_exist,
	MOJE_TOTAL                 
};

enum MOJE_DECYZJE {
	MOJE_DECYZJE_BREAK,
	MOJE_DECYZJE_CONTINUE,
	MOJE_DECYZJE_WARNING
};

enum MOJE_DECYZJE moje_bledy(enum MOJE_KOMUNIKATY message);

#endif 
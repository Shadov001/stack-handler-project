#include "stdafx.h"


static char *kierunki_str_tab[] = { 
	"Informatyka",			
	"Matematyka",			
	"Fizyka"		
};

#pragma warning (disable : 4996)

void MY_STUDENT_kierunek(enum KIERUNEK mess) 
{
	printf("%s \n", kierunki_str_tab[mess]);
}

void *MY_STUDENT_Initialize(char *nname, size_t rozmiar, size_t rok, enum KIERUNEK kierunek)
{
	MY_STUDENT *pdat = (MY_STUDENT *)malloc(sizeof(MY_STUDENT));
	if (pdat)
	{
		pdat->nazwisko = (char *)malloc(rozmiar * sizeof(char));
		if (!(pdat->nazwisko))
			moje_bledy(MOJE_KOMUNIKATY_allocate_error);
		memcpy(pdat->nazwisko, nname, rozmiar);
		pdat->rozmiar_nazw = rozmiar;
		pdat->rok = rok;
		pdat->kierunek = kierunek;
	}
	else
		moje_bledy(MOJE_KOMUNIKATY_allocate_error);
	return (void *)(pdat);
}


void MY_STUDENT_Free(void *ptr) 

{
	MY_STUDENT *pDat = (MY_STUDENT *)ptr;
	if (pDat) {
		if (pDat->nazwisko) { 
			free(pDat->nazwisko);
			pDat->nazwisko = NULL;
		}
		free(pDat);
		pDat = NULL;
	}
}

void * MY_STUDENT_Push(char *nazwisko, size_t rozmiarnazwisko, size_t rok, enum KIERUNEK kierunek)
{
	return MY_STUDENT_Initialize(nazwisko, rozmiarnazwisko, rok, kierunek);
}

void MY_STUDENT_Print(void *ptr)
{
	MY_STUDENT *p = (MY_STUDENT *)ptr;
	if (p)
	{
		printf("NAZWISKO:   %s\n", p->nazwisko);
	    printf("ROK:        %zd\n", p->rok);
		printf("KIERUNEK:   ");
		MY_STUDENT_kierunek(p->kierunek); 
	}
}

void MY_STUDENT_Compare(void *ptr, char * string ,int *retvalue) {
	MY_STUDENT *p = (MY_STUDENT *)ptr;
	retvalue = 0;
	if (!(strcmp(p->nazwisko, string))) {
		printf("Znaleziono element!\n");
		retvalue++;
		MY_STUDENT_Print(ptr);
	}
}

void MY_STUDENT_Write(void *ptr, FILE *plik) {
	MY_STUDENT *p = (MY_STUDENT *)ptr;

	if (!fwrite(p, sizeof(MY_STUDENT), 1, plik)) //ZAPIS CALEGO KONTENERA
		moje_bledy(MOJE_KOMUNIKATY_save_error);
	if (!fwrite(p->nazwisko, sizeof(char), p->rozmiar_nazw, plik)) // ZAPIS NAZWISKA PO ODCZYTANYM JEGO ROZMIARZE
		moje_bledy(MOJE_KOMUNIKATY_save_error);
	
}

void * MY_STUDENT_Read(FILE *plik) 
{
	//SF Po zapisie i zamykaniu programu odczyt dziala blednie. (POPRAWIONE)

	MY_STUDENT *retval = NULL;
	char nowe_nazwisko[512];
	size_t rozmiar_nazwiska;

	retval = (MY_STUDENT *)malloc(sizeof(MY_STUDENT));
	if (!retval)
	{
		moje_bledy(MOJE_KOMUNIKATY_allocate_error);
	}
	memset(retval, 0, sizeof(MY_STUDENT));

	if (!fread(retval, sizeof(MY_STUDENT), 1, plik))
		moje_bledy(MOJE_KOMUNIKATY_read_error);

	//SF tu mozna sproscic. Odczytuje Pan z dysku nowe_nazwisko i znajduje jego dlugosc.
	//dalej alokujemy pamiec retval->nazwisko i kopiujemy retval->nazwisko <-- nowe_nazwisko.  (POPRAWIONE)

	retval->nazwisko = (char *)malloc(retval->rozmiar_nazw * sizeof(char));
	if (!fread(retval->nazwisko, sizeof(char), retval->rozmiar_nazw, plik))
		moje_bledy(MOJE_KOMUNIKATY_read_error);
	return(void*)retval;
}

#ifndef MY_INTERFACE___H
#define MY_INTERFACE___H

enum MY_INTERF_EN
{
	PUSH,
	POP,
	WRITE,
	READ,
	ALL,
	FIND,
	CLEAR,
	STOP,
	TOT
};

void _info();
void _menu();
void _read(char f[]);
void _write(char f[]);
void _push();
void _pop();
void _printall();
void _clear();
void _find();
void _std_clean();
void workdir(char filepathname[]);
#endif